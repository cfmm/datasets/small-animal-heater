# Small-animal heater

A negative feedback circuit to control the temperature of a heating blanket using the Model 1025 MR-compatible Small Animal Monitoring Gating System. This can be used to control the body temperature of small animals within the MRI environment.
'heater_report.docx' provides an overview of the project and a description of all included files.
This project was completed by Mark Keran at the CFMM.